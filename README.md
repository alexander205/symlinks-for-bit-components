# Symlinks For Bit Components

Create symlinks for bit components

## Projects structure should be next

- symlinks-for-bit-components
- bit-component-library
- mfe-reactclone
- mfe-shell
- user-engagement-service
- user-management-service

## Example of use

1) Write needed packages into "packages.txt" file, for example

`@parlor/dashboard.components.data-table.pagination`

`@parlor/dashboard.components.data-table.table`

2) Then link

`cd symlinks-for-bit-components`

`node link.js user-management-service`

3) Unlink if needed

`cd symlinks-for-bit-components`

`node unlink.js user-management-service`


**YOU WILL NEED TO RESTART REPOSITORY AFTER UNLINK**