const path = require('path')
const { execSync } = require('child_process')

const { processLineByLine, log } = require('./_common');

(async function main () {
    const bitModulesToLink = await processLineByLine(path.resolve(__dirname + '/packages.txt'))

    const repositoryFolderName = (process.argv.slice(2))[0];
    if (!repositoryFolderName) {
      return;
    }

    // step 1
    process.chdir(path.resolve(path.join(__dirname, '..') + '/bit-component-library/node_modules'));
    const bitComponentLibrary = process.cwd();

    if (bitModulesToLink.length) {
        log('STEP #1 STARTED')

        bitModulesToLink.forEach((bitModuleToLink) => {
            const pathToBitModule = bitComponentLibrary + '/' + bitModuleToLink;

            process.chdir(pathToBitModule);
            execSync('npm link')

            log('cd ' + pathToBitModule)
            log('npm link')
        })
        log('STEP #1 COMPLETED')
        log('-----');
    }

    // step 2
    process.chdir(path.resolve(path.join(__dirname, '..') + '/' + repositoryFolderName));
    const repositoryToLink = process.cwd();

    if (bitModulesToLink.length) {
        log('STEP #2 STARTED')
        log('cd ' + repositoryToLink)

        bitModulesToLink.forEach((bitModuleToLink) => {
            execSync('npm link ' + bitModuleToLink)
            log('npm link ' + bitModuleToLink)
        })
        log('STEP #2 COMPLETED')
        log('-----');
    }

})()