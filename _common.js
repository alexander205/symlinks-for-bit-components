const path = require('path')
const { createReadStream } = require('fs')
const { once } = require('events');
const { createInterface } = require('readline');

async function processLineByLine(pathToBitModulesToLink) {
    try {
        const rl = createInterface({
        input: createReadStream(pathToBitModulesToLink),
        crlfDelay: Infinity
        });

        const lines = []
        rl.on('line', (line) => {
            lines.push(line)
        });

        await once(rl, 'close');

        return lines
    } catch (err) {
        console.error(err);
    }
}

const log = (...args) => {
    console.log('\x1b[36m', ...args ,'\x1b[0m')
}

module.exports = {
    processLineByLine,
    log
}

